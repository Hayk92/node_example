const express       = require('express'),
      cors          = require('cors'),
      mongoose      = require('mongoose'),
      compression   = require('compression'),
      {cache}       = require('@helpers'),
      bodyParser    = require('body-parser'),
      jsonParser    = bodyParser.json(),
      createError   = require('http-errors'),
      logger        = require('morgan'),

      {user, auth, withdraw, deposit, transaction}
                    = require('@routes');
                      require('dotenv').config();

let app = express();

app.use(compression());

let corsOptions = {
    origin:  function (origin, callback) {
        let whitelist = [
            process.env.WHITE_LIST
        ];
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Access denied!'))
        }
    },
    optionsSuccessStatus: 204
};

app.use(jsonParser);
app.use(cors(corsOptions));
app.use(function(err, req, res, next) {
    res.status(403).json({error: "Access denied!"});
});

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: 'application/*+json' }));

app.use('/api/v1/users', user);
app.use('/api/v1/auth', auth);
app.use('/api/v1/withdraws', cache, withdraw);
app.use('/api/v1/deposits', cache, deposit);
app.use('/api/v1/transactions', cache, transaction);
app.use(logger('dev'));

app.use(function(req, res, next) {
    next(createError(404));
});

app.use(function(err, req, res, next) {
    // // set locals, only providing error in development
    // res.locals.message = err.message;
    // res.locals.error = req.app.get('env') === 'development' ? err : {};
    //
    // // response the error
    // res.status(err.status || 500).json('error');
});

mongoose
    .connect(`mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true
    }).then(() => {
        console.log("Successfully connected to the database");
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now...', err);
        process.exit();
    });

module.exports = app;
