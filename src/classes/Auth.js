const jwt       = require('jsonwebtoken'),
    request     = require('request'),
    {Session}   = require('@modules');

let identify = null;

class Auth {
    static async login(login, password) {
        return await new Promise((resolve, reject) => {
            request.post({
                url:`${process.env.EXCHANGER_API_URL}/${process.env.API_PROVIDER}/login`,
                form: {
                    login, password
                }
            }, async (err, res, body) =>{
                let data = JSON.parse(body);
                data.access_token = await jwt.sign({id: data.user._id}, process.env.EXCHANGER_API_SECRET, {expiresIn: process.env.TOKEN_EXPIRE});
                identify = await data.user._id;
                await Session.auth.set(data.user._id, data);
                resolve(data);
            })
        });
    }

    static user() {
        return Session.auth.get(identify);
    }

    static check() {
        return Session.auth.get(identify);
    }

    static async refresh() {
        return await new Promise((resolve, reject) => {
            let auth = Session.auth.get(identify);
            request.post({
                url: `${process.env.EXCHANGER_API_URL}/${process.env.API_PROVIDER}/refresh-token`,
                form: {
                    refresh_token: auth.refresh_token
                }
            }, (err, res, body) => {
                let data = JSON.parse(body);
                auth.token = data.token;
                auth.access_token = jwt.sign({id: auth.user._id}, process.env.EXCHANGER_API_SECRET, {expiresIn: process.env.TOKEN_EXPIRE});
                resolve(data);
                return auth;
            });
        });
    }

    static logout() {
        Session.auth.delete(identify);
    }
}

module.exports = Auth;
