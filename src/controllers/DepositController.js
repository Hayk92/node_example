const {Auth}  = require('@classes'),
      request = require('request'),
      {redis} = require('@modules');

module.exports = {
    get: (req, res) => {
        request.get({
            headers: {
                Authorization: Auth.user().token
            },
            url: `${process.env.EXCHANGER_API_URL}/${process.env.APP_PROVIDER}/deposits?page=${req.query.page}`
        },(err, response, body) => {
            let data = JSON.parse(body);
            redis.setex(req.originalUrl, 3600, body);
            res.status(200).json(data);
            res.flush();
        });
    }
};
