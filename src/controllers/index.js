const AuthController        = require('./AuthController'),
      WithdrawController    = require('./WithdrawController'),
      DepositController     = require('./DepositController'),
      UserController        = require('./UserController'),
      TransactionController = require('./TransactionController');

module.exports = {
    AuthController,
    WithdrawController,
    DepositController,
    TransactionController,
    UserController
};
