const {user}              = require('@models'),
      jwt                 = require('jsonwebtoken'),
      {UserResource}      = require('@resources'),
      {Validator}         = require('@modules');

module.exports = {
    login: (req, res, next) => {
        (new Validator(res)).validate(req, () => {
            user.findOrFail({email: req.body.email}, (err, user) => {
                if (err)
                    return res.status(422).json({errors: {email: "Invalid credentials!"}});
                user.comparePassword(req.body.password, async (err, isMatch) => {
                    if (isMatch) {
                        user.access_token = jwt.sign({id: user._id}, req.app.get("secretKey"), {expiresIn: '1h'});
                        user.refresh_token = jwt.sign({id: user._id}, req.app.get("secretKeyRefresh"), {expiresIn: +process.env.REFRESH_TOKEN_EXPIRE});
                        return res.status(200).json(UserResource(user));
                    } else
                        return res.status(422).json({errors: {email: "Invalid credentials!"}});
                })
            });
        });
    },

    refreshToken: (req, res, next) => {
        if (req.body.refresh_token) {
            return res.status(200)
                .json({
                    access_token : jwt.sign({id: req.body.id}, req.app.get("secretKey"), {expiresIn: '1h'})
                });
        }
        return res.status(401).json({"message": "Unauthorized!"});
    },

    update: (req, res, next) => {
        (new Validator(res)).validate(req, () => {
            user.findOneAndUpdate({_id: req.decoded.id},
                {
                    $set: {
                        username:   req.body.username,
                        email:      req.body.email,
                    }
                }, {new: true, useFindAndModify: false},  (err, user) => {
                    return res.status(200).json(UserResource(user));
                }
            );
        });
    },

    updatePassword: (req, res, next) => {
        (new Validator(res)).validate(req, () => {
            user.findOneAndUpdate({_id: req.decoded.id},
                {
                    $set: {
                        password:   req.body.password
                    }
                }, {new: true, useFindAndModify: false},  (err, user) => {
                    return res.status(200).json(UserResource(user));
                }
            );
        });
    }
};
