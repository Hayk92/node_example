const {user}        = require('@models'),
      {Validator}   = require('@modules');

module.exports = {
    users: (req, res, next) => {
        let limit = 13,
            page = +req.query.page * limit,
            options = {
                started_at: -1,
                offset: page,
                limit: limit
            };

        user.paginate({"_id": {"$ne": req.decoded.id}}, options, (err, users) => {
            return res.status(200).json(users);
        });
    },
    create: (req, res, next) => {
        (new Validator(res)).validate(req, () => {
            let created_user = new user({
                username:   req.body.username,
                email:      req.body.email,
                password:   req.body.password,
            });
            created_user.save(err => {
                return res.status(200).json(created_user);
            })
        });
    },
    delete: (req, res, next) => {
        user.findByIdAndRemove(req.params.id, (err, result) => {
            return res.status(200).json(result);
        });
    }
};
