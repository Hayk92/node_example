#!/usr/bin/env node
const {transaction} = require('@models'),
      importer      = require('./importer'),
      [,, args]     = process.argv;

let provider = args || null;

importer(transaction, provider, 'transactions');

