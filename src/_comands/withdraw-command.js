#!/usr/bin/env node
const {withdraw} = require('@models'),
      importer   = require('./importer'),
      [,, args]  = process.argv;

let provider = args || null;

importer(withdraw, provider, 'withdraws');
