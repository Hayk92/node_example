const request       = require('request'),
      mongoose      = require('mongoose');
                      require('dotenv').config();

module.exports = function (model, provider, endpoint) {
    request(`${process.env.EXCHANGER_API_URL}/${provider}/${endpoint}`,(err, res, body) => {
        mongoose.connect(`mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        }).then(() => {
            let data = JSON.parse(body);
            return model.create(data);
        }).then(response => {
            console.log("Done.");
            process.exit();
        }).catch(err => {
            console.log("Woops something is wrong!");
            process.exit();
        });
    });
};
