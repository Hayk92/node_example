#!/usr/bin/env node
const {deposit} = require('@models'),
      importer  = require('./importer'),
      [,, args] = process.argv;

let provider = args || null;

importer(deposit, provider, 'deposits');
