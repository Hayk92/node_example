const UserResource      = require('./userResource'),
      WithdrawResource  = require('./withdrawResource');

module.exports = {
    UserResource,
    WithdrawResource
};
