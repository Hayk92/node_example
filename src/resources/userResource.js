module.exports = function(data) {
    return {
        _:              data._id,
        username:       data.username,
        email:          data.email,
        access_token:   data.access_token,
        refresh_token:  data.refresh_token
    }
};
