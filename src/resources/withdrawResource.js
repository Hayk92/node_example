module.exports = function (data) {
    console.log(data);
    return {
        amount          : data.amount,
        bank            : data.bank,
        player_name     : data.full_name,
        account_number  : data.account_number,
        status          : data.status
    }
};
