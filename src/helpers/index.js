const cache = require('@helpers/cache');

module.exports = {
    errMessage : (messages) => {
        let invalid = {};
        messages.map((err) => {
            if (!invalid[err.param]) invalid[err.param] = [];
            return invalid[err.param].push(err.msg);
        });
        return invalid;
    },
    cache
};
