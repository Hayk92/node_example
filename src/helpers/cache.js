module.exports = (req, res, next) => {
    const {redis} = require('@modules');
    const org = req.originalUrl;
    redis.get(org, (err, data) => {
        if (data) {
            return res.status(200).json(JSON.parse(data));
        } else {
            next();
        }
    });
};
