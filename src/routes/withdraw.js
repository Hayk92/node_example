const express               = require('express'),
      router                = express.Router(),
      {WithdrawController}  = require('@controllers'),
      bodyParser            = require('body-parser'),
      jsonParser            = bodyParser.json();

router.use(require('@middlewares/tokenChecker'));
router.use(require('@middlewares/s2sTokenChecker'));

router.get('/', jsonParser, WithdrawController.get);

module.exports = router;
