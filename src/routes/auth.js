const express           = require('express'),
      router            = express.Router(),
      bodyParser        = require('body-parser'),
      jsonParser        = bodyParser.json(),
      {UpdateUser, UpdatePasswordUser, CreateUser}    = require('@requests'),
      {AuthController, UserController}  = require('@controllers');

router.use(require('@middlewares/tokenChecker'));

router.put('/update/:id', jsonParser, UpdateUser, AuthController.update)
    .put('/update-password/:id', jsonParser, UpdatePasswordUser, AuthController.updatePassword)
    .get('/users', jsonParser, UserController.users)
    .post('/create', jsonParser, CreateUser, UserController.create)
    .delete ('/users/:id', jsonParser, UserController.delete);

module.exports = router;
