const express           = require('express'),
      router            = express.Router(),
      bodyParser        = require('body-parser'),
      jsonParser        = bodyParser.json(),
      {LoginRequest}    = require('@requests'),
      {AuthController}  = require('@controllers');

router.post('/login', jsonParser, LoginRequest, AuthController.login);
router.post('/refresh-token', jsonParser, AuthController.refreshToken);

module.exports = router;
