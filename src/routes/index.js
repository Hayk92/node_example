const user          = require('./user'),
      auth          = require('./auth'),
      withdraw      = require('./withdraw'),
      deposit       = require('./deposit'),
      transaction   = require('./transaction');

module.exports = {
    user,
    auth,
    withdraw,
    deposit,
    transaction
};
