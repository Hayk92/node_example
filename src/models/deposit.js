const mongoose = require('mongoose'),
      model    = require('@models/model');

let DepositSchema = model({
    user_id:        {
        type: String,
        // type:       Schema.Types.ObjectId,
        // ref:        'Accounts',
        required:   true
    },
    received_deposit_id: {
        type:       Number,
        required:   true
    },
    amount:         {
        type:       Number,
        required:   true,
    },
    bank:         {
        type:       String,
        required:   true,
    },
    currency:       {
        type:       String,
        default:    'IRT'
    },
    cvv_csv:        {
        type:       String,
    },
    card_number:    {
        type:       String
    },
    account_number:    {
        type:       String
    },
    expire_at:      {
        type:       Date,
        default:    Date.now
    },
    status:         {
        type:       String,
        default:    'pending'
    }
});

module.exports = mongoose.model('Deposits', DepositSchema);
