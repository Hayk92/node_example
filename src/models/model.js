const mongoose  = require('mongoose'),
    paginate    = require('mongoose-paginate');

function schema (settings) {
    let Schema = new mongoose.Schema(settings);

    Schema.plugin(paginate);

    Schema.statics.findOrFail = function (fields, callback) {
        return this.findOne(fields).exec(function(err, data) {
            if (!data) {
                return callback(new Error('Not Found!').message, null);
            }
            return callback(err, data);
        });
    };

    return Schema;
}

module.exports = schema;
