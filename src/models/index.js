const user          = require('./user'),
      withdraw      = require('./withdraw'),
      deposit       = require('./deposit'),
      transaction   = require('./transaction');

module.exports = {
    user,
    transaction,
    withdraw,
    deposit
};
