const mongoose    = require('mongoose'),
      bcrypt      = require('bcrypt'),
      saltRounds  = 10,
      model       = require('@models/model');

let UserSchema = model({
    username: {
        type:       String,
        required:   true,
    },
    email: {
        type:       String,
        required:   true,
        unique:     true,
        trim:       true,
        index:      true
    },
    password: {
        type:       String,
        required:   true,
        trim:       true
    },
    role: {
        type:       mongoose.Schema.Types.ObjectId,
        ref:        'Roles',
        required:   true,
        default:    "5dc9420dc5253b0efd068996"
    }
});

UserSchema.pre('save', function (next) {
    if (!this.isModified('password')) { return next(); }
    this.password = bcrypt.hashSync(this.password, saltRounds);
    next();
});

UserSchema.pre('findOneAndUpdate', function(next) {
    if (this.getUpdate().$set.password){
        this.findOneAndUpdate({}, {password: bcrypt.hashSync(this.getUpdate().$set.password, saltRounds)});
    }
    next();
});

UserSchema.methods.comparePassword = function(reqPassword, callback) {
    bcrypt.compare(reqPassword, this.password, (err, isMatch) => {
        if (err) { return callback(err); }

        callback(null, isMatch);
    });
};

module.exports = mongoose.model('Users', UserSchema);
