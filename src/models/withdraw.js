const mongoose = require('mongoose'),
      model    = require('@models/model');

let WithdrawSchema = model({
    user_id:        {
        type: String,
        // type:       Schema.Types.ObjectId,
        // ref:        'Accounts',
        // required:   true
    },
    received_withdraw_id: {
        type:       Number,
        // required:   true
    },
    full_name:       {
        type:       String,
        // required:   true,
    },
    bank:         {
        type:       String,
        // required:   true,
    },
    amount:         {
        type:       Number,
        // required:   true,
    },
    currency:       {
        type:       String,
        default:    'IRT'
    },
    card_number:    {
        type:       String,
        // required:   true
    },
    account_number:    {
        type:       String,
        // required:   true
    },
    swift_code:    {
        type:       Number,
        // required:   true
    },
    status:         {
        type:       String,
        default:    'pending'
    },
    expire_at:      {
        type:       Date,
        default:    Date.now()
    },
});

module.exports = mongoose.model('Withdraws', WithdrawSchema);
