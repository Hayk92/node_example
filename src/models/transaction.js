const mongoose    = require('mongoose'),
      model       = require('@models/model'),
      pagination  = require('mongoose-paginate');

let TransactionSchema = model({
    deposit_id:     {
        type: String,
        // type:       Schema.Types.ObjectId,
        // ref:        'Deposits',
        required:   true,
    },
    withdraw_id:    {
        type: String,
        // type:       Schema.Types.ObjectId,
        // ref:        'Withdraws',
    },
    code:           {
        type:       String,
        required:   true,
        default:    Math.random().toString(36).substring(2)
    },
    amount:         {
        type:       Number,
        required:   true,
    },
    started_at:     {
        type:       Date,
        required:   true,
        default:    Date.now,
    },
    finished_at:     {
        type:       Date
    },
    expire_at:      {
        type:       Date,
        default:    Date.now
    },
    status:         {
        type:       String,
        default:    'completed'
    }
});

TransactionSchema.plugin(pagination);

module.exports = mongoose.model('Transactions', TransactionSchema);
