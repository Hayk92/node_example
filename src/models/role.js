const mongoose  = require('mongoose'),
      model     = require('@models/model');

let RoleSchema = model({
    role: {
        type:       String,
        required:   true
    },
    key: {
        type:       Number,
        required:   true,
        default:    2
    }
});

module.exports = mongoose.model('Roles', RoleSchema);
