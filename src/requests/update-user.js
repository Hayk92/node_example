const {check} = require('express-validator');

const updateUserRequest = [
    check('email').exists().withMessage('Email address is required!')
        .custom(value => {
            return !!value
        }).withMessage('Email address is required!')
        .isEmail().withMessage('Email address is invalid!'),
    check('username').exists().withMessage('Username is required!')
        .custom(value => {
            return !!value
        }).withMessage('Username is required!')
];

module.exports = updateUserRequest;
