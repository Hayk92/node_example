const {check} = require('express-validator');

const createUserRequest = [
    check('email').exists().withMessage('Email address is required!')
        .custom(value => {
            return !!value
        }).withMessage('Email address is required!')
        .isEmail().withMessage('Email address is invalid!'),
    check('username').exists().withMessage('Username is required!')
        .custom(value => {
            return !!value
        }).withMessage('Username is required!'),
    check('password').exists().withMessage('Password is required!')
        .custom((value, {req}) => {
            return value === req.body.password_confirmation;
        }),
    check('password_confirmation').exists().withMessage('Password Confirmation is required!')
        .custom((value, {req}) => {
            return value === req.body.password;
        }),
];

module.exports = createUserRequest;
