const LoginRequest         = require('./login-request'),
      UpdateUser           = require('./update-user'),
      UpdatePasswordUser   = require('./update-user-password');
      CreateUser           = require('./create-user');

module.exports = {
    LoginRequest,
    UpdateUser,
    UpdatePasswordUser,
    CreateUser
};
