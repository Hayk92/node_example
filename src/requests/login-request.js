const {check} = require('express-validator');

const loginRequest = [
    check('email').exists().withMessage('Email address is required!')
        .isEmail().withMessage('Email address is invalid!'),
    check('password').exists().withMessage('Password is required!')
];

module.exports = loginRequest;
