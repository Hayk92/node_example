const {check} = require('express-validator');

const updateUserPasswordRequest = [
    check('password').exists().withMessage('Password is required!')
        .custom(value => {
            return !!value
        }).withMessage('Password is required!'),
    check('password_confirmation').exists().withMessage('Password confirmation is required!')
        .custom(value => {
            return !!value
        }).withMessage('Password confirmation is required!')
        .custom((value, {req}) => {
            return value === req.body.password
        }).withMessage('Password confirmation must match!')
];

module.exports = updateUserPasswordRequest;
