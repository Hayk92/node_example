const {validationResult}  = require('express-validator');

class Validator {
    constructor(res) {
        this.response   = res;
    }

    validate(req, cb) {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            return this.response.status(422).json({errors: this.errMessage(errors.array())});

        cb();
    }

    errMessage(messages) {
        let invalid = {};
        messages.map((err) => {
            if (!invalid[err.param]) invalid[err.param] = [];
            return invalid[err.param].push(err.msg);
        });
        return invalid;
    }
}

module.exports = Validator;
