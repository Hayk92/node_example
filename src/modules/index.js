const Validator     = require('./vlaidator'),
      Session       = require('./globals/session'),
      redis         = require('./globals/redis');

module.exports = {
    Validator,
    Session,
    redis
};
