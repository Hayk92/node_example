const redis  = require('redis'),
      client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);

client.on('error', err => {
    console.log(`ERROR ${err}`);
});

module.exports = client;
