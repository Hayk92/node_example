let hashMapSession = {};

exports.auth = auth = {
    set: function (key, value) {
        hashMapSession[key] = value;
    },
    get: function (key) {
        return hashMapSession[key];
    },
    delete: function (key) {
        delete hashMapSession[key];
    },
    all: function () {
        return hashMapSession;
    }
};
