const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    const token = req.headers['authorization'];

    if (token) {
        jwt.verify(token, req.app.get("secretKey"), function (err, decoded) {
            if (err) {
                return res.status(401).json({"message": "Unauthorized aaa!"});
            }
            req.decoded = decoded;
            next()
        });
    } else {
        return res.status(401).json({"message": "Unauthorized zzz!"});
    }
};
