const jwt    = require('jsonwebtoken'),
      {Auth} = require('@classes');

module.exports = async (req, res, next) => {
    if (await Auth.check()) {
        await jwt.verify(Auth.user().token, process.env.EXCHANGER_SECRET_KEY, async function (err, decoded) {
            if (err) {
                await Auth.refresh()
                    .then(async res => {
                        let result = await JSON.stringify(res);
                    })
                    .then(() => {
                        return next();
                    });
            }
            req.decoded = decoded;
            next();
        });
    } else {
        await Auth.login(process.env.EXCHANGER_API_LOGIN, process.env.EXCHANGER_API_PASSWORD)
            .then(async (res) => {
                let result = await JSON.stringify(res);
            })
            .then(() => {
                next();
            });
    }
};
